FROM ubuntu:xenial
MAINTAINER Yehuda Deutsch yehuda@hasadna.org.il

RUN apt-get update
RUN apt-get install -y openssh-server python software-properties-common sudo
RUN mkdir /var/run/sshd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN mkdir /root/.ssh \
    && echo "# REPLACE THIS WITH YOUR PUBLIC SSH KEY #" > /root/.ssh/authorized_keys

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
